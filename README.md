## Jejif API
API for Jejif Application

## Installation

```bash
$ yarn install
```

## Running the app

Start homebrew installed postgre 
```bash
brew services start postgresql
```

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## Setup eslint
When eslint is setup properly, it should check and fixed your git staged file for errors.

1. Run `yarn install` to install dependencies
2. Run `yarn run prepare-husky` to install/setup husky. 
   -  It will create `.husky` folder in the root folder. Make sure you it generated `.husky/pre-commit` file. If not, you can create my running `npx husky add .husky/pre-commit` Then add this content.
   ```
    #!/bin/sh
    . "$(dirname "$0")/_/husky.sh"

    yarn lint-staged
    ```

## Seeders

Transaction : Run `npx nestjs-command transaction:seed`

## Creating a Seeder
Seeding data uses [nestjs-command](https://www.npmjs.com/package/nestjs-command) package.
See example implementation in `UserSeederCommand`.


### Seeding user
To seed user run `npx nestjs-command user:seed`. 
Add `--help` to see available help.
This will create new user. See implementation in `UserSeederCommand`.
