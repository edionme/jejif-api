import { EntityRepository, Repository } from 'typeorm';
import { SignUpUserInput } from './dto/signup-user.input';
import { User } from './entities/user.entity';
import { UpdateUserInput } from './dto/update-user.input';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async createUser(createUserInput: SignUpUserInput): Promise<User> {
    const { name, email, phone } = createUserInput;
    const user = new User();
    user.name = name;
    user.email = email;
    user.phone = phone;
    await user.save();
    return user;
  }

  async updateUser(
    user: User,
    updateUserInput: UpdateUserInput,
  ): Promise<User> {
    const { name, phone } = updateUserInput;
    user.name = name ?? user.name;
    user.phone = phone ?? user.phone;
    await user.save();
    return user;
  }
}
