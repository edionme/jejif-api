import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { UsersService } from './users.service';
import { User } from './entities/user.entity';
import { UpdateUserInput } from './dto/update-user.input';
import { FilterUserInput } from './dto/filter-user.input';
import { UsePipes, ValidationPipe } from '@nestjs/common';
import { UserType } from './user.type';

@Resolver(() => UserType)
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @UsePipes(ValidationPipe)
  @Query(() => [UserType], { name: 'users' })
  async users(
    @Args('filterUserInput') filterUserInput: FilterUserInput,
  ): Promise<User[]> {
    return await this.usersService.getAllUsers(filterUserInput);
  }

  @Query(() => UserType, { name: 'user' })
  async findOne(@Args('id', { type: () => Int }) id: number): Promise<User> {
    return await this.usersService.findOne(id);
  }

  @UsePipes(ValidationPipe)
  @Mutation(() => UserType)
  updateUser(
    @Args('id', { type: () => Int }) id: number,
    @Args('updateUserInput') updateUserInput: UpdateUserInput,
  ) {
    return this.usersService.update(id, updateUserInput);
  }

  @Mutation(() => UserType)
  removeUser(@Args('id', { type: () => Int }) id: number) {
    return this.usersService.remove(id);
  }
}
