import { Comment } from 'src/comments/entities/comment.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Index('uniq_user_email', { unique: true })
  @Column()
  email: string;

  @Index('uniq_user_phone', { unique: true })
  @Column()
  phone: string;

  @OneToMany(() => Comment, (comment) => comment.user, {
    nullable: true,
    eager: true,
  })
  comments: Comment[];

  @CreateDateColumn({ type: 'date' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'date' })
  updatedAt: Date;
}
