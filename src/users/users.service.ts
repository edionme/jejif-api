import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SignUpUserInput } from './dto/signup-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { UserRepository } from './user.repository';
import { User } from './entities/user.entity';
import { FilterUserInput } from './dto/filter-user.input';
import { FindOperator, Like } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {}
  async create(signUpUserInput: SignUpUserInput): Promise<User> {
    try {
      return await this.userRepository.createUser(signUpUserInput);
    } catch (error) {
      if (error.code === '23505') {
        if (error.constraint === 'uniq_user_email') {
          throw new BadRequestException('Email address is already used.');
        } else {
          throw new BadRequestException('Phone is already already used.');
        }
      }

      throw new Error('Error creating user account');
    }
  }

  async getAllUsers(filterUserInput: FilterUserInput): Promise<User[]> {
    const { name } = filterUserInput;

    interface WhereInterface {
      name?: FindOperator<string>;
    }

    const where: WhereInterface = {};
    if (name) {
      where.name = Like(`%${name}%`);
    }

    return await this.userRepository.find({
      where,
      order: {
        id: 'DESC',
      },
    });
  }

  async findOne(id: number): Promise<User> {
    const user = await this.userRepository.findOne(id);

    if (!user) {
      throw new NotFoundException('User not found.');
    }

    return user;
  }

  async update(id: number, updateUserInput: UpdateUserInput): Promise<User> {
    const user = await this.findOne(id);
    return await this.userRepository.updateUser(user, updateUserInput);
  }

  async remove(id: number): Promise<User> {
    const user = await this.findOne(id);
    await this.userRepository.delete(id);
    return user;
  }

  async findByPhone(phone: string): Promise<User> {
    const user = await this.userRepository.findOne({ where: { phone } });
    if (!user) {
      throw new NotFoundException('User not found.');
    }

    return user;
  }
}
