import { Injectable } from '@nestjs/common';
import { Command } from 'nestjs-command';
import { UsersService } from '../users.service';

@Injectable()
export class UserSeederCommand {
  constructor(private readonly usersService: UsersService) {}

  @Command({
    command: 'user:seed',
    describe: 'Seed user',
    autoExit: true,
  })
  async seed() {
    try {
      const user = await this.usersService.create({
        name: 'John Foe',
        email: 'johnfoe@jejif.com',
        phone: '09109978934',
      });

      console.info('New user created', user);
    } catch (error) {
      console.error(error);
    }
  }
}
