import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class UserType {
  @Field()
  id: number;

  @Field()
  name: string;

  @Field({ nullable: true })
  email: string;

  @Field()
  phone: string;

  @Field()
  createdAt: string;

  @Field()
  updatedAt: string;
}
