const rand = (min: number, max: number) => {
  const random = Math.random();
  return Math.floor(random * (max - min) + min);
};

export const otpGenerator = (length: number) => {
  const digits = '123456789';
  let password = '';
  while (password.length < length) {
    const charIndex = rand(0, digits.length - 1);
    password += digits[charIndex];
  }
  return parseInt(password);
};
