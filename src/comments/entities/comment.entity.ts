import { Exclude } from 'class-transformer';
import { Max, Min } from 'class-validator';
import { Transaction } from 'src/transactions/entities/transaction.entity';
import { User } from 'src/users/entities/user.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class Comment extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  content: string;

  @Column()
  @Min(1)
  @Max(5)
  ratings: number;

  @ManyToOne(() => User, (user) => user.comments, {
    onDelete: 'CASCADE',
    nullable: true,
    eager: false,
  })
  @Exclude({ toPlainOnly: true })
  user: User;

  @ManyToOne(() => Transaction, (transaction) => transaction.comments, {
    onDelete: 'CASCADE',
    nullable: true,
    eager: false,
  })
  @Exclude({ toPlainOnly: true })
  transaction: number;

  @CreateDateColumn({ type: 'date' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'date' })
  updatedAt: Date;
}
