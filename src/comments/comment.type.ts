import { ObjectType, Field } from '@nestjs/graphql';
import { TransactionType } from 'src/transactions/transaction.type';
import { UserType } from 'src/users/user.type';

@ObjectType()
export class CommentType {
  @Field()
  id: number;

  @Field()
  content: string;

  @Field()
  ratings: number;

  @Field(() => UserType, { nullable: true })
  user: UserType;

  @Field(() => TransactionType, { nullable: true })
  transaction: TransactionType;

  @Field()
  createdAt: string;

  @Field()
  updatedAt: string;
}
