import { User } from 'src/users/entities/user.entity';
import {
  EntityRepository,
  Equal,
  FindOperator,
  Like,
  Repository,
} from 'typeorm';
import { CreateCommentInput } from './dto/create-comment-input';
import { FilterCommentInput } from './dto/filter-comment.input';
import { UpdateCommentInput } from './dto/update-comment-input';
import { Comment } from './entities/comment.entity';
@EntityRepository(Comment)
export class CommentsRepository extends Repository<Comment> {
  async createComment(
    createCommentInput: CreateCommentInput,
    user: User,
  ): Promise<Comment> {
    const { content, ratings, transaction } = createCommentInput;
    const comment = new Comment();
    comment.content = content;
    comment.ratings = ratings;
    comment.user = user;
    comment.transaction = transaction;

    await comment.save();
    return comment;
  }
  async getAllComments(
    filterCommentInput: FilterCommentInput,
  ): Promise<Comment[]> {
    const { content, ratings } = filterCommentInput;
    interface WhereInterface {
      content?: FindOperator<string>;
      ratings?: FindOperator<number>;
    }

    const where: WhereInterface = {};
    if (content) {
      where.content = Like(`%${content}%`);
    }

    if (ratings) {
      where.ratings = Equal(ratings);
    }

    return this.find({
      where,
      order: {
        id: 'DESC',
      },
    });
  }

  async updateComment(
    comment: Comment,
    updateCommentInput: UpdateCommentInput,
  ): Promise<Comment> {
    const { content, ratings } = updateCommentInput;
    comment.content = content ?? comment.content;
    comment.ratings = ratings ?? comment.ratings;
    await comment.save();
    return comment;
  }
}
