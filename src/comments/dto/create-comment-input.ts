import { InputType, Field, Int } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class CreateCommentInput {
  @IsNotEmpty()
  @Field()
  content: string;

  @IsNotEmpty()
  @Field()
  ratings: number;

  @IsNotEmpty()
  @Field()
  transaction: number;
}
