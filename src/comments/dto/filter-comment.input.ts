import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class FilterCommentInput {
  @Field({ nullable: true })
  content: string;

  @Field({ nullable: true })
  ratings: number;

  @Field(() => Int, { nullable: true })
  user: number;
}
