import { UsePipes, ValidationPipe } from '@nestjs/common';
import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UserRepository } from 'src/users/user.repository';
import { CommentType } from './comment.type';
import { CommentsService } from './comments.service';
import { CreateCommentInput } from './dto/create-comment-input';
import { FilterCommentInput } from './dto/filter-comment.input';
import { UpdateCommentInput } from './dto/update-comment-input';
import { Comment } from './entities/comment.entity';

@Resolver(() => CommentType)
export class CommentsResolver {
  constructor(
    private readonly commentsService: CommentsService,
    private readonly userRepository: UserRepository,
  ) {}

  @UsePipes(ValidationPipe)
  @Mutation(() => CommentType)
  async createComment(
    @Args('createCommentInput') createCommentInput: CreateCommentInput,
  ): Promise<Comment> {
    /** TODO:
     * = get the user from current user logged in
     */

    const user = await this.userRepository.find({ take: 1 });
    return this.commentsService.createComment(createCommentInput, user[0]);
  }

  @UsePipes(ValidationPipe)
  @Query(() => [CommentType], { name: 'comments' })
  async comments(
    @Args('filterCommentInput') filterCommentInput: FilterCommentInput,
  ): Promise<Comment[]> {
    return this.commentsService.getAllComments(filterCommentInput);
  }

  @Query(() => CommentType, { name: 'comment' })
  async findOne(@Args('id', { type: () => Int }) id: number): Promise<Comment> {
    return await this.commentsService.findOne(id);
  }

  @UsePipes(ValidationPipe)
  @Mutation(() => CommentType)
  async updateComment(
    @Args('id', { type: () => Int }) id: number,
    @Args('updateCommentInput') updateCommentInput: UpdateCommentInput,
  ): Promise<Comment> {
    return this.commentsService.updateComment(id, updateCommentInput);
  }

  @Mutation(() => CommentType)
  removeComment(@Args('id', { type: () => Int }) id: number) {
    return this.commentsService.remove(id);
  }
}
