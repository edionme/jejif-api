import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Equal, FindOperator, Like } from 'typeorm';
import { CommentsRepository } from './comments.repository';
import { CreateCommentInput } from './dto/create-comment-input';
import { FilterCommentInput } from './dto/filter-comment.input';
import { UpdateCommentInput } from './dto/update-comment-input';
import { Comment } from './entities/comment.entity';

@Injectable()
export class CommentsService {
  constructor(
    @InjectRepository(CommentsRepository)
    private commentRepository: CommentsRepository,
  ) {}

  async createComment(
    createCommentInput: CreateCommentInput,
    user: User,
  ): Promise<Comment> {
    return this.commentRepository.createComment(createCommentInput, user);
  }

  async findOne(id: number): Promise<Comment> {
    const comment: Comment = await this.commentRepository.findOne(id);

    if (!comment) {
      throw new NotFoundException('Comment not found.');
    }

    return comment;
  }

  async getAllComments(
    filterCommentInput: FilterCommentInput,
  ): Promise<Comment[]> {
    const { content, ratings } = filterCommentInput;
    interface WhereInterface {
      content?: FindOperator<string>;
      ratings?: FindOperator<number>;
    }

    const where: WhereInterface = {};
    if (content) {
      where.content = Like(`%${content}%`);
    }

    if (ratings) {
      where.ratings = Equal(ratings);
    }

    return this.commentRepository.find({
      where,
      relations: ['user', 'transaction'],
      order: {
        id: 'DESC',
      },
    });
  }

  async updateComment(
    id: number,
    updateCommentInput: UpdateCommentInput,
  ): Promise<Comment> {
    const comment: Comment = await this.findOne(id);
    return this.commentRepository.updateComment(comment, updateCommentInput);
  }

  async remove(id: number): Promise<Comment> {
    const comment = await this.findOne(id);
    await this.commentRepository.delete(id);
    return comment;
  }
}
