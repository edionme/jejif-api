import { Comment } from 'src/comments/entities/comment.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { Status, Type } from '../transaction.type';

@Entity()
export class Transaction extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  customer: number;

  @Column()
  worker: number;

  @Column({
    type: 'enum',
    enum: Status,
    default: Status.OPEN,
  })
  status: Status;

  @Column({
    type: 'enum',
    enum: Type,
    default: Type.FIND_HELP,
  })
  type: Type;

  @Column()
  logs: string;

  @OneToMany(() => Comment, (comment) => comment.transaction, {
    nullable: true,
    eager: true,
  })
  comments: Comment[];

  @CreateDateColumn({ type: 'date' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'date' })
  updatedAt: Date;
}
