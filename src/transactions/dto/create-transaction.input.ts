import { InputType, Field } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';
import { Status, Type } from '../transaction.type';

@InputType()
export class CreateTransactionInput {
  @IsNotEmpty()
  @Field()
  customer: number;

  @IsNotEmpty()
  @Field()
  worker: number;

  @IsNotEmpty()
  @Field()
  status: Status;

  @IsNotEmpty()
  @Field()
  type: Type;
}
