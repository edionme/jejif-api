import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class FilterTransactionInput {
  @Field()
  status: string;

  @Field()
  type: string;
}
