import { CreateTransactionInput } from './create-transaction.input';
import { InputType, Field, PartialType } from '@nestjs/graphql';
import { Status, Type } from '../transaction.type';

@InputType()
export class UpdateTransactionInput extends PartialType(
  CreateTransactionInput,
) {
  @Field()
  customer: number;

  @Field()
  worker: number;

  @Field()
  status: Status;

  @Field()
  type: Type;
}
