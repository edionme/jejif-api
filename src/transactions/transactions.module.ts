import { Module } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { TransactionsResolver } from './transactions.resolver';
import { TransactionRepository } from './transaction.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransactionSeederCommand } from './commands/transaction.seeder.command';

@Module({
  imports: [TypeOrmModule.forFeature([TransactionRepository])],
  providers: [
    TransactionsResolver,
    TransactionsService,
    TransactionSeederCommand,
  ],
})
export class TransactionsModule {}
