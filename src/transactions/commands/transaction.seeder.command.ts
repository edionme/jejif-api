import { Injectable } from '@nestjs/common';
import { Command } from 'nestjs-command';
import { TransactionsService } from '../transactions.service';
import { Status, Type } from '../transaction.type';

@Injectable()
export class TransactionSeederCommand {
  constructor(private readonly transactionService: TransactionsService) {}

  @Command({
    command: 'transaction:seed',
    describe: 'Seed transactions',
    autoExit: true,
  })
  async seed() {
    try {
      const newDatas = [
        {
          customer: 1,
          worker: 2,
          type: Type.FIND_HELP,
          status: Status.DONE,
        },
        {
          customer: 2,
          worker: 3,
          status: Status.IN_PROGRESS,
          type: Type.HELP_ME,
        },
        {
          customer: 3,
          worker: 1,
          status: Status.DONE,
          type: Type.HIRE_ME,
        },
        {
          customer: 2,
          worker: 3,
          status: Status.CANCELLED,
          type: Type.FIND_HELP,
        },
      ];
      for (let x = 0; x < newDatas.length; x++) {
        const transaction = await this.transactionService.create(newDatas[0]);
        console.info('New transaction created', transaction);
      }
    } catch (error) {
      console.error(error);
    }
  }
}
