import { EntityRepository, Repository } from 'typeorm';
import { CreateTransactionInput } from './dto/create-transaction.input';
import { Transaction } from './entities/transaction.entity';
import { UpdateTransactionInput } from './dto/update-transaction.input';
import { Status } from './transaction.type';

@EntityRepository(Transaction)
export class TransactionRepository extends Repository<Transaction> {
  async createTransaction(
    createTransactionInput: CreateTransactionInput,
  ): Promise<Transaction> {
    const {
      customer,
      worker,
      type,
      status = Status.OPEN,
    } = createTransactionInput;

    const transaction = new Transaction();
    transaction.customer = customer;
    transaction.worker = worker;
    transaction.type = type;
    transaction.status = status;
    transaction.logs = '';

    await transaction.save();
    return transaction;
  }

  async updateTransaction(
    transaction: Transaction,
    updateTransactionInput: UpdateTransactionInput,
  ): Promise<Transaction> {
    const { customer, worker, type, status } = updateTransactionInput;
    transaction.customer = customer ?? transaction.customer;
    transaction.worker = worker ?? transaction.worker;
    transaction.type = type ?? transaction.type;
    transaction.status = status ?? transaction.status;
    await transaction.save();
    return transaction;
  }
}
