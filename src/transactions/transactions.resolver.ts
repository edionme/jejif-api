/* eslint-disable prettier/prettier */
import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { TransactionsService } from './transactions.service';
import { Transaction } from './entities/transaction.entity';
import { CreateTransactionInput } from './dto/create-transaction.input';
import { UpdateTransactionInput } from './dto/update-transaction.input';
import { UsePipes, ValidationPipe } from '@nestjs/common';
import { FilterTransactionInput } from './dto/filter-transaction.input';
import { TransactionType } from './transaction.type';

@Resolver(() => Transaction)
export class TransactionsResolver {
  constructor(private readonly transactionsService: TransactionsService) {}

  @UsePipes(ValidationPipe)
  @Mutation(() => TransactionType)
  async createTransaction(
    @Args('createTransactionInput') createTransactionInput: CreateTransactionInput,
  ): Promise<Transaction> {
    return await this.transactionsService.create(createTransactionInput);
  }

  @UsePipes(ValidationPipe)
  @Query(() => [TransactionType], { name: 'transactions' })
  async transactions(
    @Args('filterTransactionInput') filterTransactionInput: FilterTransactionInput,
  ): Promise<Transaction[]> {
    return await this.transactionsService.getAllTransactions(
      filterTransactionInput,
    );
  }

  @Query(() => TransactionType, { name: 'transaction' })
  async findOne(
    @Args('id', { type: () => Int }) id: number,
  ): Promise<Transaction> {
    return await this.transactionsService.findOne(id);
  }

  @UsePipes(ValidationPipe)
  @Mutation(() => TransactionType)
  updateTransaction(
    @Args('id', { type: () => Int }) id: number,
    @Args('updateTransactionInput') updateTransactionInput: UpdateTransactionInput,
  ) {
    return this.transactionsService.update(id, updateTransactionInput);
  }

  @Mutation(() => TransactionType)
  removeTransaction(@Args('id', { type: () => Int }) id: number) {
    return this.transactionsService.remove(id);
  }
}
