import { ObjectType, Field } from '@nestjs/graphql';

export enum Status {
  OPEN = 'Open',
  IN_PROGRESS = 'In-Progress',
  DONE = 'Done',
  CANCELLED = 'Cancelled',
}

export enum Type {
  FIND_HELP = 'Find Help',
  HIRE_ME = 'Hire Me',
  HELP_ME = 'Help Me',
}

@ObjectType()
export class TransactionType {
  @Field()
  id: number;

  @Field()
  customer: number;

  @Field()
  worker: number;

  @Field()
  status: Status;

  @Field()
  type: Type;

  @Field()
  createdAt: string;

  @Field()
  updatedAt: string;
}
