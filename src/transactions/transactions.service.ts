import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Like } from 'typeorm';

import { CreateTransactionInput } from './dto/create-transaction.input';
import { FilterTransactionInput } from './dto/filter-transaction.input';
import { UpdateTransactionInput } from './dto/update-transaction.input';
import { Transaction } from './entities/transaction.entity';
import { TransactionRepository } from './transaction.repository';

@Injectable()
export class TransactionsService {
  constructor(
    @InjectRepository(TransactionRepository)
    private transactionRepository: TransactionRepository,
  ) {}

  async create(
    createTransactionInput: CreateTransactionInput,
  ): Promise<Transaction> {
    try {
      return await this.transactionRepository.createTransaction(
        createTransactionInput,
      );
    } catch (error) {
      throw new Error('Error creating transaction');
    }
  }

  async getAllTransactions(
    filterTransactionInput: FilterTransactionInput,
  ): Promise<Transaction[]> {
    const { status, type } = filterTransactionInput;

    interface WhereInterface {
      status?: FindOperator<string>;
      type?: FindOperator<string>;
    }

    const where: WhereInterface = {};
    if (status) {
      where.status = Like(`%${status}%`);
    }
    if (type) {
      where.type = Like(`%${type}%`);
    }

    return await this.transactionRepository.find({
      where,
      order: {
        id: 'DESC',
      },
    });
  }

  async findOne(id: number): Promise<Transaction> {
    const transaction = await this.transactionRepository.findOne(id);

    if (!transaction) {
      throw new NotFoundException('Transaction not found.');
    }

    return transaction;
  }

  async update(
    id: number,
    updateTransactionInput: UpdateTransactionInput,
  ): Promise<Transaction> {
    const transaction = await this.findOne(id);
    return await this.transactionRepository.updateTransaction(
      transaction,
      updateTransactionInput,
    );
  }

  async remove(id: number): Promise<Transaction> {
    const transaction = await this.findOne(id);
    await this.transactionRepository.delete(id);
    return transaction;
  }
}
