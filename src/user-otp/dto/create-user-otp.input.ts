import { InputType, Field } from '@nestjs/graphql';
import { User } from '../../users/entities/user.entity';

@InputType()
export class CreateUserOtpInput {
  @Field()
  user: User;

  @Field()
  otp: number;

  @Field()
  expireAt: Date;
}
