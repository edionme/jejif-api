import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserOtpInput } from './dto/create-user-otp.input';
import { UserOtpRepository } from './user-otp.repository';
import { otpGenerator } from '../utils/otp-generator';
import { User } from '../users/entities/user.entity';
import { UserOtp } from './entities/user-otp.entity';
import { LessThan, Not } from 'typeorm';
import { ValidateOtpInput } from '../auth/dto/validate-otp-input';

@Injectable()
export class UserOtpService {
  constructor(
    @InjectRepository(UserOtpRepository)
    private userOtpRepository: UserOtpRepository,
  ) {}
  async create(user: User): Promise<number> {
    const otp = otpGenerator(4);
    const expireAt = new Date();
    expireAt.setMinutes(expireAt.getMinutes() + 2);
    const createUserOtpInput: CreateUserOtpInput = {
      expireAt,
      user,
      otp,
    };

    await this.userOtpRepository.createUserOtp(createUserOtpInput);
    return otp;
  }

  async findByOtp(validateOtpInput: ValidateOtpInput): Promise<UserOtp> {
    const { userId, otp } = validateOtpInput;
    return await this.userOtpRepository.findOne({
      where: {
        otp,
        expireAt: Not(LessThan(new Date())),
        user: { id: userId },
      },
      relations: ['user'],
    });
  }
}
