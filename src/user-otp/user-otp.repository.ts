import { EntityRepository, InsertResult, Repository } from 'typeorm';
import { UserOtp } from './entities/user-otp.entity';
import { CreateUserOtpInput } from './dto/create-user-otp.input';

@EntityRepository(UserOtp)
export class UserOtpRepository extends Repository<UserOtp> {
  async createUserOtp(
    createUserOtpInput: CreateUserOtpInput,
  ): Promise<InsertResult> {
    const { user, otp, expireAt } = createUserOtpInput;
    const userOtp = await this.createQueryBuilder()
      .insert()
      .into(UserOtp)
      .values({
        otp,
        expireAt,
        user,
      })
      .onConflict(
        `("userId") DO UPDATE SET "otp" = excluded.otp
        ,"createdAt" = excluded."createdAt"
        ,"expireAt" = excluded."expireAt"`,
      )
      .execute();

    return userOtp;
  }
}
