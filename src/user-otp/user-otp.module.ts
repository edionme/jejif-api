import { Module } from '@nestjs/common';
import { UserOtpService } from './user-otp.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserOtpRepository } from './user-otp.repository';

@Module({
  imports: [TypeOrmModule.forFeature([UserOtpRepository])],
  providers: [UserOtpService],
})
export class UserOtpModule {}
