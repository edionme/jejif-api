export interface JwtPayloadDto {
  id: number;
  name: string;
  email?: string;
  phone: string;
  createdAt: Date;
  updatedAt: Date;
}
