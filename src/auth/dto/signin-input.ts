import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class SignInInput {
  @Field()
  phone: string;
}
