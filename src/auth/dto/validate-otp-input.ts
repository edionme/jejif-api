import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class ValidateOtpInput {
  @Field()
  otp: number;

  @Field()
  userId: number;
}
