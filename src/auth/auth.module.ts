import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from './auth.service';
import { AuthResolver } from './auth.resolver';
import { UsersService } from 'src/users/users.service';
import { UserRepository } from '../users/user.repository';
import { UserOtpService } from '../user-otp/user-otp.service';
import { UserOtpRepository } from '../user-otp/user-otp.repository';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    ConfigModule.forRoot(),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: {
        expiresIn: '1m',
      },
    }),
    TypeOrmModule.forFeature([UserRepository, UserOtpRepository]),
  ],
  providers: [
    AuthService,
    UsersService,
    AuthResolver,
    UserOtpService,
    JwtStrategy,
  ],
})
export class AuthModule {}
