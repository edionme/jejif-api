import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { ValidationPipe, UsePipes, UseGuards } from '@nestjs/common';
import { UserType } from '../users/user.type';
import { SignUpUserInput } from '../users/dto/signup-user.input';
import { User } from '../users/entities/user.entity';
import { AuthService } from './auth.service';
import { SignInInput } from './dto/signin-input';
import { ValidateOtpInput } from './dto/validate-otp-input';
import { JwtResponseType } from './jwt.response.type';
import { GqlAuthGuard } from './gql-auth.guard';
import { UsersService } from '../users/users.service';
import { CurrentUser } from './current-user.decorator';

@Resolver()
export class AuthResolver {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @UsePipes(ValidationPipe)
  @Mutation(() => UserType)
  async signUp(
    @Args('signUpUserInput') signUpUserInput: SignUpUserInput,
  ): Promise<User> {
    return await this.authService.signUp(signUpUserInput);
  }

  @Query(() => UserType)
  async signIn(@Args('signInInput') signInInput: SignInInput): Promise<User> {
    return await this.authService.signIn(signInInput);
  }

  @Query(() => JwtResponseType)
  async validateOtp(
    @Args('validateOtpInput') validateOtpInput: ValidateOtpInput,
  ): Promise<{ access_token: string }> {
    return await this.authService.validateOtp(validateOtpInput);
  }

  @Query(() => UserType)
  @UseGuards(GqlAuthGuard)
  async whoAmI(@CurrentUser() user: User): Promise<User> {
    return await this.usersService.findOne(user.id);
  }
}
