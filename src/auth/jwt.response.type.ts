import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class JwtResponseType {
  @Field()
  access_token: string;
}
