import { Injectable, NotFoundException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { User } from '../users/entities/user.entity';
import { SignUpUserInput } from '../users/dto/signup-user.input';
import { UserOtpService } from '../user-otp/user-otp.service';
import { SignInInput } from './dto/signin-input';
import { ValidateOtpInput } from './dto/validate-otp-input';
import { JwtService } from '@nestjs/jwt';
import { JwtPayloadDto } from './dto/jwt.payload.dto';
import { UserOtp } from '../user-otp/entities/user-otp.entity';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private userOtpService: UserOtpService,
    private jwtService: JwtService,
  ) {}

  async signUp(createUserInput: SignUpUserInput): Promise<User> {
    const user = await this.usersService.create(createUserInput);
    await this.userOtpService.create(user);
    return user;
  }

  async signIn(signInInput: SignInInput): Promise<User> {
    const { phone } = signInInput;
    const user = await this.usersService.findByPhone(phone);

    await this.userOtpService.create(user);

    // TODO
    // should send an otp via sms
    return user;
  }

  async validateOtp(
    validateOtpInput: ValidateOtpInput,
  ): Promise<{ access_token: string }> {
    const userOtp = await this.userOtpService.findByOtp(validateOtpInput);

    if (!userOtp) {
      throw new NotFoundException('Invalid Otp');
    }

    const payload: JwtPayloadDto = { ...userOtp.user };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
